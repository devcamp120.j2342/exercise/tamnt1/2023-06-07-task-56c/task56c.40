package invoice.customerinvoiceapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import invoice.customerinvoiceapi.models.Customer;

@Service
public class CustomerService {
    public List<Customer> createCustomer() {
        ArrayList<Customer> customerList = new ArrayList<>();
        Customer customer1 = new Customer(1, "Tam", 10);
        Customer customer2 = new Customer(2, "adam", 20);
        Customer customer3 = new Customer(3, "Khan", 20);

        customerList.addAll(Arrays.asList(customer1, customer2, customer3));

        return customerList;
    }
}
