package invoice.customerinvoiceapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import invoice.customerinvoiceapi.models.Customer;
import invoice.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService {

    public List<Invoice> createInvoice() {
        ArrayList<Customer> customerList = new ArrayList<>();
        ArrayList<Invoice> invoiceList = new ArrayList<>();

        Customer customer1 = customerList.get(0);
        Customer customer2 = customerList.get(1);
        Customer customer3 = customerList.get(2);
        Invoice invoice1 = new Invoice(1, customer1, 10000);
        Invoice invoice2 = new Invoice(1, customer2, 20000);
        Invoice invoice3 = new Invoice(1, customer3, 30000);
        invoiceList.addAll(Arrays.asList(invoice1, invoice2, invoice3));

        for (Invoice invoice : invoiceList) {
            System.out.println(invoice);
        }

        return invoiceList;

    }
}
