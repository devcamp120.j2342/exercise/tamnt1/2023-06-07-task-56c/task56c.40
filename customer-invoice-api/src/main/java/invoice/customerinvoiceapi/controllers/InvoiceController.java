package invoice.customerinvoiceapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import invoice.customerinvoiceapi.models.Invoice;
import invoice.customerinvoiceapi.services.InvoiceService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class InvoiceController {
    @Autowired
    private InvoiceService service;

    @GetMapping("/invoices")
    public List<Invoice> getInvoices() {
        return service.createInvoice();
    }
}
